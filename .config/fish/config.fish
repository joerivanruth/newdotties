#

if not set -q FISH_ENV
	set -gx FISH_ENV 1
	set PATH ~/bin/sh ~/bin/(uname -m) ~/bin ~/opt/bin ~/.cargo/bin ~/.npm-packages/bin /home/jvr/lib/pyenv/shims ~/.local/bin ~/lib/pyenv/bin $PATH

	set -x MANWIDTH 80

	set -qU VISUAL
	or set -xU VISUAL vim

	set -qU MONETDB_ENVS
	or set -xU MONETDB_ENVS $HOME/monets

	set -qU BAT_THEME
	or set -xU BAT_THEME ansi

	set -qU LESSOPEN
	or set -xU LESSOPEN \x7c/usr/bin/lesspipe\x20\x25s

	set -qU PKG_CONFIG_PATH
	or set -xU PKG_CONFIG_PATH $HOME/lib/fake-pkg-config

	set -qU LESSPIPE
	or set -xU LESSPIPE \x1d

	set -qU SETUVAR
	or set -U fish_color_command \x2d\x2dbold

	set -qU SETUVAR
	or set -U fish_color_end brmagenta

	set -qU SETUVAR
	or set -U fish_color_escape brmagenta

	set -qU SETUVAR
	or set -U fish_color_match \x2d\x2dbackground\x3dbrblue

	set -qU SETUVAR
	or set -U fish_color_operator green

	set -qU SETUVAR
	or set -U fish_color_quote brcyan

	set -qU SETUVAR
	or set -U fish_color_redirection brblue

	set -qU SETUVAR
	or set -U fish_pager_color_completion \x1d

	set -qU SETUVAR
	or set -U fish_pager_color_description B3A06D\x1eyellow

	set -qU SETUVAR
	or set -U fish_pager_color_prefix white\x1e\x2d\x2dbold\x1e\x2d\x2dunderline

	if set -q USER
		mkdir -p /tmp/"$USER"
	end
	set -x XMODIFIERS ''
end

if command -s -q pyenv
	pyenv init - | source
end

bind \cx show_status

bind \cj save_command_buffer_then_execute
bind \cm save_command_buffer_then_execute
bind \ea restore_command_buffer

bind \eq push_command_buffer_stack


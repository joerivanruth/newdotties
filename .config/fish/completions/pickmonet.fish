# function _complete_monets
# 	if not set -q MONETDB_ENVS
# 		return
# 	end
# 
# 	for d in (ls $MONETDB_ENVS)
# 		set -l dir $MONETDB_ENVS/$d
# 		if test -d $dir/inst
# 			if set -l desc (git -C $dir/src describe --dirty --all 2>/dev/null)
# 				echo -e "$d\t$desc"
# 			else if test -d $dir/src
# 				echo -e "$d"
# 			else
# 				echo -e "$d\t<<no src dir!>>"
# 			end
# 		end
# 	end
# end
# 
complete -c pickmonet -f -a '(_complete_monets)'

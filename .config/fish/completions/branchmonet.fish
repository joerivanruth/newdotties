function _complete_branchmonet
	set -l args (commandline -opc)

	set -q MONETDB_SRC
	or return

	set -q MONETDB_ENVS
	or return

	switch (count $args)
	case 1
		# first argument is a git rev
		git -C $MONETDB_SRC for-each-ref refs/tags refs/heads refs/remotes --format='%(refname:short)'
	case 2
		# second argument is the trailing component of the first,
		# unless that already exists under $MONETDB_ENVS
		set -l name (string replace -r '_release$' '' (basename $args[2]))
		echo branches/$name
	end
end

complete -c branchmonet -f -a '(_complete_branchmonet)'

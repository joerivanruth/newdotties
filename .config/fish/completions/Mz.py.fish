function _complete_Mz_py
	set -l args (commandline -opc)

	switch (count $args)
	case 1
		list-mtests --dirs --full-dirs
	case '*'
		set -l prefix (dirname $args[2])
		list-mtests | sed -n -e "s,^$prefix/,,p"
	end
end

complete -c Mz.py -f -a '(_complete_Mz_py)'

#complete -c Mz.py -f -a '(list-mtests --dirs --full-dirs)'

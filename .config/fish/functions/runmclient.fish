function runmclient --argument port filename
	mclient -p $port -fraw $filename | sed -e '/^[#%]/d' -e '/^function/i\ ';
end

# Defined in /tmp/fish.R7CI3o/buildntest.fish @ line 2
function buildntest
	argparse -n buildntest -x b,f,t b/build f/build-fast t/only-test l/lock r/release -- $argv
	or return
	set -l release_flag
	if set -q _flag_r
		set release_flag --release --debinfo
	end

	if set -q _flag_t
		# do not rebuild at all
		true
	else if set -q _flag_f
		# rebuild but do not reconfigure
		time ionice -c idle nice -29 buildmonet
	else  # -b
		ionice -c idle nice -29 configuremonet $release_flag -DASSERT=ON -DSANITIZER=ON
		echo
		time ionice -c idle nice -29 buildmonet
	end

	set -l lockfile "$MONETDB_DIR"
	if set -q _flag_l
		set lockfile "$MONETDB_ENVS"
	end

	flock --verbose -o $lockfile ionice -c idle nice -29 Mtest.py $argv

	set -l stat $status
	if test $stat = 0
		notify-send -t 2500 "SUCCEEDED: "(basename $MONETDB_DIR)
	else
		notify-send -t 2500 "FAILED [$stat]: "(basename $MONETDB_DIR)
	end
	return $stat
end

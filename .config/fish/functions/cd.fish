# Defined in /tmp/fish.IhCTa9/cd.fish @ line 2
function cd --description 'Change directory'
	set -l MAX_DIR_HIST 25
    set -l dest
    set -l inplace false

    switch (count $argv)
	case 0
	   set dest ~
	case 1
	    set dest $argv
	case 2
	    if test $argv[1] = '-'
		set dest $argv[2]
		set inplace true
	    else
	        printf "%s\n" (_ "Too many args for cd command")
		return 1
	    end
	case '*'
	    printf "%s\n" (_ "Too many args for cd command")
	    return 1
    end

    # Skip history in subshells.
    if status --is-command-substitution
        builtin cd $argv
        return $status
    end

    # Avoid set completions.
    set -l previous $PWD

    if test "$argv" = "-"
        if test "$__fish_cd_direction" = "next"
            nextd
        else
            prevd
        end
        return $status
    end

    # allow explicit "cd ." if the mount-point became stale in the meantime
    if test "$argv" = "."
        cd "$PWD"
        return $status
    end

    if test -f $dest
        set dest (dirname $dest)
        echo "$dest"
    end

    builtin cd $dest
    set -l cd_status $status

    if test $cd_status -eq 0 -a "$PWD" != "$previous" -a "$inplace" != true
        set -q dirprev
        or set -l dirprev
        set -q dirprev[$MAX_DIR_HIST]
        and set -e dirprev[1]
        set -g dirprev $dirprev $previous
        set -e dirnext
        set -g __fish_cd_direction prev
    end

    return $cd_status
end

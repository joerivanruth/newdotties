function pickmonet
	argparse s/src -- $argv
	set -l env $argv[1]

	if not set -q MONETDB_ENVS
		echo "MONETDB_ENVS not set"
		return 1
	end
	set -l instdir $MONETDB_ENVS/$env/inst

	if not test -d $instdir
		echo "$instdir does not exist"
		return 1
	end

	function update_path
		set -l var "$argv[1]"
		set -l env "$argv[2]"
		set -l suffix "$argv[3]"
		set -l items $$var
		set -l idx 0
		if test -n "$MONETDB_DIR"
			for i in (seq (count $items))
				switch $items[$i]
				case "$MONETDB_DIR/*"
					set idx $i
					break
				end
			end
		end
		if test -z "$env"
			if test $idx -eq 0
				echo "Nothing to remove in $var"
			else
				echo "Removing $var""[$idx]=$items[$idx]"
				set -e items[$idx]
			end
		else
			set -l value "$MONETDB_ENVS/$env/$suffix"
			if test $idx -eq 0
				echo "Prepending $value to $var"
				set items $value $items
			else if test "$value" = "$items[$idx]"
				echo "Keeping $var""[$idx]"
			else
				echo "Replacing $var""[$idx] with $value"
				set items[$idx] $value
			end
		end
		if count $items >/dev/null
			set -x -g $var $items
		else
			echo "Removing $var altogether"
			set -e $var
		end
	end
	update_path PATH "$env" inst/bin
	update_path LD_LIBRARY_PATH "$env" inst/lib
	update_path PKG_CONFIG_PATH "$env" inst/lib/pkgconfig
	update_path MONETDBE_INCLUDE_PATH "$env" inst/include/monetdb
	update_path MONETDBE_LIBRARY_PATH "$env" inst/lib

	if [ -d $instdir/lib ]
		set -l pythons (find $instdir/lib -maxdepth 1 -type d -name python3\* )
		if count $pythons >/dev/null
			update_path PYTHONPATH "$env" inst/lib/(basename $pythons[1])/site-packages
		end
	end

	if test -n "$env"
		set -g -x MONETDB_DIR $MONETDB_ENVS/$env
		set -g -x ODBCSYSINI "$MONETDB_DIR"
		set -U MONETDB_ENV_LATEST $env
		if set -q _flag_src
			cd "$MONETDB_DIR/src"
		end
	else
		set -e MONETDB_DIR
		set -e ODBCSYSINI
	end
end

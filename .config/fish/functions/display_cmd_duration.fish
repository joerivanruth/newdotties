# Defined in /tmp/fish.ixHiua/display_cmd_duration.fish @ line 2
function display_cmd_duration
	set -l x (math $argv[1] / 1000)
        set -l hours (math -s0 "$x / 3600")
        set -l minutes (math -s0 "$x % 3600 / 60")
        set -l seconds (math "$x % 60")
        if [ $hours -gt 0 ]
                printf "%dh%02dm%04.1fs\n" $hours $minutes $seconds
        else if [ $minutes -gt 0 ]
                printf "%2dm%04.1fs\n" $minutes $seconds
        else if [ $seconds -ge 10 ]
                printf "%.2fs\n" $seconds
        else
                printf "%1.3fs\n" $seconds
        end
end

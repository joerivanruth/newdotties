# Defined in /tmp/fish.6bXRI1/cded2.fish @ line 2
function cded2
	set -l tmpfile (mktemp -t cd.XXXXXXXXX)
	or return 1
	for line in $dirprev
		echo -s '' "  $line" >>$tmpfile
	end
	echo -s '' ". $PWD" >>$tmpfile
	for line in $dirnext
		echo -s '' "  $line" >>$tmpfile
	end

	vim $tmpfile

	set -l lines (cat $tmpfile)
	rm -f $tmpfile
	
	if not count $lines >/dev/null
		return
	end
	
	set -l my_dirprev
	set -l my_dirnext
	set -l my_cwd
	set -l state before
	for line in $lines
		set -l prefix (string sub -l 2 $line)
		set -l path (string sub -s 3 $line)
		if [ x$prefix = x". " -a -n $path ]
			if [ x$state != xbefore ]
				return 1
			end
			set my_cwd $path
			set state after
		else
			switch $state
			case before
				set -a my_dirprev $path
			case after
				set -a my_dirnext $path
			case *
				echo invalid state
				return 2
			end
		end
	end

	if [ x$state = xbefore ]
		# No dotted line encountered.
		# default to last entry, if possible.
		# otherwise, $PWD
		if count $my_dirprev >/dev/null
			set my_cwd $my_dirprev[-1]
			set -e my_dirprev[-1]
		else
			set my_cwd $PWD
		end
	end

	#echo "< $my_dirprev | $my_cwd | $my_dirnext >"
	echo "PREV: $my_dirprev"
	echo "NOW:  $my_cwd"
	echo "NEXT: $my_dirnext"

	# if contains - $lines
	# 	set -l idx (contains -i - $lines)
	# 	set dirprev $lines[1..$idx]
	# 	set dirnext 4lines[$idx..-1]
	# 	set -e dirprev[-1]
	# 	set -e dirnext[1]
	# else
	# 	set dirprev $lines
	# 	set dirnext
	# end
end

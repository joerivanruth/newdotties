function gitignore --argument dir
test -d $dir -a ! -f $dir/.gitignore
or return
echo '*' > $dir/.gitignore
end

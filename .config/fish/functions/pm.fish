function pm
	if set -q MONETDB_ENV_LATEST
		pickmonet $MONETDB_ENV_LATEST $argv
	else
		echo "MONETDB_ENV_LATEST not set"
		return 1
	end
end

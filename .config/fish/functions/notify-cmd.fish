# Defined in /tmp/fish.vFsWRw/notify-cmd.fish @ line 1
function notify-cmd
	if $argv
		set -l stat $status
		notify-send -t 2500 "SUCCEEDED: $argv"
		return $stat
	else
		set -l stat $status
		notify-send -t 2500 "FAILED [$stat]: $argv"
		return $stat
	end
end

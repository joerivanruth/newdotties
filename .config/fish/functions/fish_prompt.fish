# Defined in /tmp/fish.KYVCJQ/fish_prompt.fish @ line 2
function fish_prompt
	set -l saved_status $status

	set -q status_line
	or set -g status_line

	set -q fish_prompt_char
	or set -g fish_prompt_char "»"

	if test -n "$status_line"
		display_status_line $status_line
		set status_line
	end

	if [ $saved_status = 0 ]
		set_color -b brgreen
		echo -n "$fish_prompt_char"
		set_color normal
	else
		set_color -b brred
		echo -n "$fish_prompt_char "
		set_color normal
	end
	echo -n " "
end

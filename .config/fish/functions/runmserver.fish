function runmserver --argument port
	set -l other_args $argv[2..]
	set -l dir "/tmp/banana$port"

	fuser -n tcp $port -k -9 || true
	rm -rf "$dir"
	mkdir -p "$dir"

	mserver5 --dbpath="$dir/demo" --set mapi_port=$port --set mapi_usock="/tmp/.s.monetdb.$port" $other_args
end

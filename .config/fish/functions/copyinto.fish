# Defined in /home/jvr/.config/fish/functions/copyinto.fish @ line 2
function copyinto --argument table
	echo ":: $table"
	argparse -x e,p 'e/explain' 'p/plan' -- $argv
	if [ -z $table ]
		echo "Usage copyinto [-e | -p] TABLE"
		return 1
	end
	set -l query "copy into $table from 'data10.csv' on client" 
	if set -q _flag_e
		set query "explain $query"
	end
	if set -q _flag_p
		set query "plan $query"
	end
	echo $query
	mclient -p 55003 -s $query | sed -e '/^[%#]/d' 
end

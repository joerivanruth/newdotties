function mergenext
	argparse -n mergenext n/dry_run -- $argv
	set -l goal $argv[1]

	test -n "$goal"
	or return 1

	set -l tgt (git rev-list --first-parent --reverse HEAD..$goal | head -1)
	if set -q _flag_n
		git show $tgt
		return
	end

	set -l msg (git log -n1 $tgt --pretty=format:'%s')
	echo "merging $tgt: $msg"
	sleep 1

	git merge $tgt -m "Merged $tgt: $msg from $goal"
	and echo (git log HEAD..main --oneline | wc -l) "commits left"
end

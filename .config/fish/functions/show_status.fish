# Defined in /tmp/fish.q1zfqs/show_status.fish @ line 2
function show_status
	set -g status_line (summarize_shell_status $status)
	commandline -f repaint
end

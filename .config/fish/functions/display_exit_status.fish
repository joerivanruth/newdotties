function display_exit_status --on-event fish_prompt
        set -l saved_status $status
        if [ -n "$previous_command_buffer_buf" -a $saved_status -gt 1 ]
                set -g status_line "[$saved_status]" (display_cmd_duration $CMD_DURATION)
        end
end

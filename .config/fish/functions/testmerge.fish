# Defined in /tmp/fish.UuaxUQ/testmerge.fish @ line 1
function testmerge

	set merged (git rev-parse --short HEAD)
	or return 1

	set parent (git rev-parse --short $merged^1)
	or return 1

	set mergee (git rev-parse --short $merged^2)
	or return 1

	echo Testing merge of $mergee into $parent giving $merged

	echo 
	pickmonet -s scratch0
	and git checkout $parent
	or return 1

	echo 
	pickmonet -s scratch1
	and git checkout $mergee
	or return 1

	echo 
	pickmonet -s scratch2
	and git checkout $merged
	or return 1
end

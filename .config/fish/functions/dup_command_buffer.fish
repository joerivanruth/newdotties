# Defined in /tmp/fish.URsciT/dup_command_buffer.fish @ line 2
function dup_command_buffer
	set -l pos (commandline -C)
	set -l buf (commandline -b)
	set -l packed "$pos $buf"

	test $packed = 0
	and return 0

	set -g -a command_line_buffer_stack $packed
	commandline -f execute
end

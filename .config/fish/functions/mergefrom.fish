# Defined in /tmp/fish.XrdTCK/mergefrom.fish @ line 2
function mergefrom --description 'Merge from given branch into current' --argument branch
	set -l branchname (basename $branch)
	set -l cur (git branch --show-current)
	set -l curname (basename $cur)
	set -l msg "Merge '$branchname' into '$curname'"
	set -l cmd git merge -m $msg $branch
	set cmd (string escape -- $cmd)
	set cmd "$cmd"
	commandline -r "$cmd"
end

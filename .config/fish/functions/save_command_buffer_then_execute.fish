function save_command_buffer_then_execute
	set -g previous_command_buffer_buf (commandline -b)
	set -g previous_command_buffer_pos (commandline -C)
	commandline -f execute
end

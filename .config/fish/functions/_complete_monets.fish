# Defined in /tmp/fish.F36PIm/_complete_monets.fish @ line 2
function _complete_monets
	if not set -q MONETDB_ENVS
		return
	end

	for d in (ls $MONETDB_ENVS)
		set -l dir $MONETDB_ENVS/$d
		if test -d $dir/inst
			if test -d $dir/src
				echo -e "$d"
			else
				echo -e "$d\t<<no src dir!>>"
			end
		end
	end
end

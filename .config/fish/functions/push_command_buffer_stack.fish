function push_command_buffer_stack
	set -l pos (commandline -C)
	set -l buf (commandline -b)
	set -l packed "$pos $buf"
	if [ $packed = "0 " ]
		# Empty, try to pop
		if [ "$command_line_buffer_stack[-1]" != "" ]
			set -l parts (string split -m 1 " " $command_line_buffer_stack[-1])
			set -l pos $parts[1]
			set -l buf $parts[2]
			commandline -b -r $buf
			commandline -C $pos
			set -e command_line_buffer_stack[-1]
		end
	else
		# If not empty, push
		set -g -a command_line_buffer_stack $packed
		commandline -b -r ''
	end
	commandline -f repaint
end

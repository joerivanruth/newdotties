# Defined in /tmp/fish.9TedKL/cded.fish @ line 2
function cded
	set -l tmpfile (mktemp -t cd.XXXXXXXXX)
	or return 1
	for line in $dirprev - $dirnext
		echo -s '' "$line" >>$tmpfile
	end

	vim $tmpfile
	set -l lines (cat $tmpfile)
	rm -f $tmpfile
	
	if not count $lines >/dev/null
		return
	end

	if contains - $lines
		set -l idx (contains -i - $lines)
		set dirprev $lines[1..$idx]
		set dirnext 4lines[$idx..-1]
		set -e dirprev[-1]
		set -e dirnext[1]
	else
		set dirprev $lines
		set dirnext
	end
end

# Defined in /tmp/fish.N9Gxmc/summarize_shell_status.fish @ line 2
function summarize_shell_status
	set -l saved_status $argv[1]
	echo "[$saved_status]"

	date '+%H:%M:%S'

	set -l envs

	if set -q VIRTUAL_ENV
		set envs $envs "venv:"(basename "$VIRTUAL_ENV")
	end

	if set -q MONETDB_DIR
		set envs $envs "mdb:"(basename "$MONETDB_DIR")
	end

	if test $SHLVL -gt 1
		set envs $envs "shlvl:$SHLVL"
	end

	if set -l bra (git symbolic-ref HEAD 2>/dev/null)
		set bra  (string replace -r '^refs/heads/' '' $bra)
		if ! contains "m:"(string replace "branches/" "" $bra) $envs
			set envs $envs "branch:$bra"
		end
		if ! git diff-index --quiet HEAD
			set envs $envs +++
		end
	else if set -l rev (git describe --all --tags --first-parent --dirty=+++ --broken 2>/dev/null)
		set envs $envs "rev:"(string replace -r '^heads/' '' $rev)
	end

	if count $envs >/dev/null
		echo "[$envs]"
	end

	if set -q hostname
		echo (id -nu)@"$hostname"
	else
		id -nu
	end

	echo "$COLUMNS"x"$LINES"

	if test -f /proc/loadavg
		cut -d' ' -f1 /proc/loadavg
	else
		echo '-.--'
	end

	if command -s -q upower
		set -l bats
		for i in (upower -e | grep _BAT)
			set -a bats (upower -i $i | sed -n -e '/percent/s/.* //p')
		end
		echo "[$bats]"
	end

	display_cmd_duration $CMD_DURATION

	echo "$PWD"
end

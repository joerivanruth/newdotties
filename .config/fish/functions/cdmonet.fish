# Defined in /tmp/fish.Hy3A0t/cdmonet.fish @ line 1
function cdmonet
	if not set -q MONETDB_DIR
		echo MONETDB_DIR not set
		return 1
	end

	cd "$MONETDB_DIR/src"
end

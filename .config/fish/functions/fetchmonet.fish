# Defined in /tmp/fish.pwQLIW/fetchmonet.fish @ line 1
function fetchmonet
	if ! set -q MONETDB_SRC
		echo '$MONETDB_SRC not set'
		return 1
	end

	if ! test -d $MONETDB_SRC
		echo '$MONETDB_SRC does not exist'
		return 1
	end

	git -C $MONETDB_SRC fetch $argv
end

function restore_command_buffer
	set -q previous_command_buffer_pos
	or set -g previous_command_buffer_pos 0

	if [ "$previous_command_buffer_buf" = "" ]
		# nothing to restore
		return 0
	end

	set -l cur (commandline -b)
	if [ $cur != "" -a $cur != "$previous_command_buffer_buf" ]
		# only restore over empty or identical command line
		return 0
	end

	# restore
	commandline -b -r $previous_command_buffer_buf
	commandline -C $previous_command_buffer_pos
	commandline -f repaint
end

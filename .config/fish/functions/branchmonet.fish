# Defined in /tmp/fish.d5yJ78/branchmonet.fish @ line 2
function branchmonet
	if ! set -q MONETDB_SRC
		echo '$MONETDB_SRC not set'
		return 1
	end

	if ! test -d $MONETDB_SRC
		echo '$MONETDB_SRC does not exist'
		return 1
	end

	git -C $MONETDB_SRC branch $argv[2] $argv[1]
end

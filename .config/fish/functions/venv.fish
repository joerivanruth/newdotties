function venv
 set -l activates {./*,./.*}/bin/activate.fish; if [ (count $activates) = 1 ]; commandline -r "source "(string escape $activates); else; echo "Pick one of $activates"; end; 
end

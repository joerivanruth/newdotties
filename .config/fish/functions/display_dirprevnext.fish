function display_dirprevnext --on-variable dirprev --on-variable dirnext
	set -l prevs (count $dirprev)
	set -l nexts (count $dirnext)
	set -l dirs (math 1 + $prevs + $nexts)
	set -l pos (math 1 + $prevs)
	set status_line "dir $pos/$dirs is $PWD "
end

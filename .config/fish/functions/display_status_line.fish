# Defined in /tmp/fish.zUegxG/display_status_line.fish @ line 2
function display_status_line
	set -l lines ""

	set -U -q fish_color_status_line
	or set -U fish_color_status_line black --background brcyan

	for a in $argv

		if [ (string length $a) -ge $COLUMNS ]
			set a (string sub -l (math $COLUMNS - 1) $a)"…"
		end

		if [ $lines[-1] = "" ]
			# Handle this case first to avoid a leading ""
			# caused by other rules
			set lines[-1] $a
			continue
		end

		if [ (string length $a) -ge $COLUMNS ]
			# so big it must go on its own line in any case
			set -a lines $a
			continue
		end

		# maybe something more will fit..
		set -l appended "$lines[-1] $a"
		if [ (string length $appended) -gt $COLUMNS ]
			set -a lines $a
			continue
		else
			set lines[-1] $appended
		end
	end

	set_color $fish_color_status_line
	for line in $lines
		printf "%-*s\n" $COLUMNS $line
	end
	set_color normal
end

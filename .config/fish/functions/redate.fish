# Defined in /tmp/fish.4K4Ym1/redate.fish @ line 2
function redate
	set -l d "$argv"
	env GIT_COMMITTER_DATE="$d" GIT_AUTHOR_DATE="$d" git commit --amend --no-edit --reset-author
end

# Defined in /tmp/fish.xddAyG/pushmonet.fish @ line 2
function pushmonet
	if ! set -q MONETDB_SRC
		echo '$MONETDB_SRC not set'
		return 1
	end

	if ! test -d $MONETDB_SRC
		echo '$MONETDB_SRC does not exist'
		return 1
	end

	git -C $MONETDB_SRC push $argv
end

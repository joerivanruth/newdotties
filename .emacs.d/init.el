;;;

;; window layout
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-splash-screen t)

;; mode line
(display-time)
(setq display-time-24hr-format t)
(line-number-mode 1)
(column-number-mode 1)

;; cursor
(blink-cursor-mode -1)
(setq x-stretch-cursor	t)
(set-cursor-color "blue")

;; fonts & colors
;(set-face-attribute 'default nil :height 128 :background "white")
(set-background-color "white")

;; misc
(setq default-major-mode 'fundamental-mode)
(setq ring-bell-function 'ignore)
(setq make-backup-files nil)
(setq track-eol t)
(setq kill-whole-line t)

;; disable ^Z.  
(global-set-key [(control z)] 'keyboard-quit)
(global-set-key (kbd "s-q") 'keyboard-quit)

;(global-set-key [(meta g)] 'goto-line)
(global-set-key [(meta J)] 'join-line)

(global-set-key "\C-x91"
		'(lambda () (interactive) (set-face-attribute 'default (selected-frame) :height 104)))
(global-set-key "\C-x92"
		'(lambda () (interactive) (set-face-attribute 'default (selected-frame) :height 128)))
(global-set-key "\C-x93"
		'(lambda () (interactive) (set-face-attribute 'default (selected-frame) :height 144)))
(global-set-key "\C-x94"
		'(lambda () (interactive) (set-face-attribute 'default (selected-frame) :height 160)))

;; open journal.org
(setq primary-journal-file nil)
(setq secondary-journal-file nil)
(setq projects-file nil)
(global-set-key "\C-cj"
		'(lambda () (interactive)
		   (if primary-journal-file
		       (find-file primary-journal-file t)
		     (message "please set primary-journal-file in ~/.emacs.d/local.el"))))
(global-set-key "\C-cJ"
		'(lambda () (interactive)
		   (if secondary-journal-file
		       (find-file secondary-journal-file t)
		     (message "please set secondary-journal-file in ~/.emacs.d/local.el"))))
(global-set-key "\C-cp"
		'(lambda () (interactive)
		   (if projects-file
		       (find-file projects-file t)
		     (message "please set projects-file in ~/.emacs.d/local.el"))))

;; disable because usually entered by accident
(put 'save-buffers-kill-terminal 'disabled t)
(put 'set-fill-column 'disabled t)
(put 'overwrite-mode 'disabled t)
(put 'narrow-to-region 'disabled nil)
(put 'org-archive-subtree 'disabled t)


(setq org-clock-mode-line-total 'today)
(setq org-clock-idle-time 15)
(setq org-clock-x11idle-program-name "x11idle")
(setq org-duration-format (quote h:mm))

;;; CUSTOM MODES
(setq load-path (cons (expand-file-name "~/lib/emacs") load-path))
(autoload 'graphviz-dot-mode "graphviz-dot-mode" "GraphViz Mode" t)
(add-to-list 'auto-mode-alist '("\\.dot\'" . graphviz-dot-mode))

(require 'org-tree-slide)

;;; MODE SPECIFIC

(add-hook 'text-mode-hook 'turn-on-auto-fill)

(setq calendar-week-start-day '1)
(setq calendar-latitude 52.378)
(setq calendar-longitude 4.906)

(setq sh-basic-offset 8)
(setq-default sh-indent-for-case-label 0)


;;; Local customizations
(load (expand-file-name "local.el" user-emacs-directory) t nil t t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(org markdown-mode))
 '(smtpmail-smtp-server "mail.monetdbsolutions.com")
 '(smtpmail-smtp-service 25))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

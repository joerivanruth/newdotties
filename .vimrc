set hidden
set ignorecase
syntax on

autocmd BufRead,BufNewFile *.fish set ai shiftwidth=8 tabstop=8 noexpandtab


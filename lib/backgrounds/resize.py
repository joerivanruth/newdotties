#!/usr/bin/python

import re
import sys

import argparse

from PIL import Image, ImageDraw


def tuple_parser(sep, n=2):
    def tup(s):
        pattern = sep.join(n * [r'([0-9]+)'])
        m = re.match(pattern, s)
        if not m:
            raise ValueError
	return tuple([int(v) for v in m.groups()])
    return tup

parser = argparse.ArgumentParser(description="Resize photo to desktop background")
parser.add_argument("source")
parser.add_argument("dest")
parser.add_argument("-s", "--size", required=True, type=tuple_parser('x'))
parser.add_argument("-c", "--center", type=tuple_parser(','), default=(50,50))
parser.add_argument("-b", "--box", type=tuple_parser(',', 4))
parser.add_argument("-t", "--trial-mode", action="store_true")

def best_box(size, box, aspect_ratio, center):
    """Returns coordinates of the largest box with the given aspect_ratio that fits
       inside the given box, centered as much as possible around the given
       center.  The center is given as a pair of numbers between 0 and 1.
       """
    # first determine the size of the box
    outer_ratio = 1.0 * (box[2]-box[0]) / (box[3] - box[1])
    if outer_ratio > aspect_ratio:
        # enclosing box is wider..
        h = box[3] - box[1]
        w = aspect_ratio * h
    else:
        # enclosing box is taller
        w = box[2] - box[0]
        h = w / aspect_ratio
    h = int(h); w = int(w)
    # fix the location of the center of the box
    xmin = box[0] + w / 2
    xmax = box[2] - w / 2
    ymin = box[1] + h / 2
    ymax = box[3] - h / 2
    x = int(max(xmin, min(xmax, 0.01 * center[0] * size[0])))
    y = int(max(ymin, min(ymax, 0.01 * center[1] * size[1])))

    # construct the box
    box = (x - w / 2, y - h / 2, x + w / 2, y + h / 2)
    return box

def make_trial_image(im, size, orig_box, box):
    mask = Image.new(im.mode, im.size)
    draw = ImageDraw.Draw(mask)
    if orig_box:
	draw.line((orig_box[0], orig_box[1]), fill='yellow', width=int(max(1, im.size[0]/100.0, im.size[1]/100.0)))
    draw.rectangle(box, outline='yellow', fill='gray')
    trial_image = Image.blend(im, mask, 0.5)
    trial_image.thumbnail(size)
    return trial_image


def make_image(im, size, box):
    result = im.copy().crop(box).resize(size, Image.ANTIALIAS)
    return result

########################################################################
args = parser.parse_args()

im = Image.open(args.source)

if args.box:
    orig_box = (
	int(0.01 * args.box[0] * im.size[0]),
	int(0.01 * args.box[1] * im.size[1]),
	int(0.01 * args.box[2] * im.size[0]),
	int(0.01 * args.box[3] * im.size[1]))
else:
    orig_box = (0, 0, im.size[0], im.size[1])

box = best_box(im.size, orig_box, 1.0 * args.size[0] / args.size[1], args.center)

if args.trial_mode:
    result = make_trial_image(im, args.size, orig_box, box)
else:
    result = make_image(im, args.size, box)

print "Writing %dx%d image to %s" % (result.size[0], result.size[1], args.dest)
result.save(args.dest, im.format, quality=99)

sys.exit()

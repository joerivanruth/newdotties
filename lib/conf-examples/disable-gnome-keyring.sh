#!/bin/sh

# it caches gpg passphrases indefinitely and is not easy
# to control

sudo sed -i -e 's/^OnlyShowIn=.*/OnlyShowIn=;/' /etc/xdg/autostart/gnome-keyring-ssh.desktop


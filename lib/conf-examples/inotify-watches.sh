#!/bin/bash

set -e -x

>>/etc/sysctl.conf echo 'fs.inotify.max_user_watches=524288'
sysctl -p

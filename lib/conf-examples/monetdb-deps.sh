#!/bin/bash

set -e -x

sudo apt install \
	bison \
	cmake \
	gcc \
	pkg-config \
	python3 \
	libbz2-dev \
	libpcre3-dev \
	libreadline-dev \
	liblzma-dev \
	zlib1g-dev \
	libasan6 \
	libcfitsio-dev \
	libcurl4-gnutls-dev \
	libgdal-dev \
	libgeos-dev \
	libnetcdf-dev \
	libproj-dev \
	libxml2-dev \
	python3-dev \
	python3-numpy \
	r-base-dev \
	unixodbc-dev \
	valgrind

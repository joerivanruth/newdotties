#!/bin/sh

set -e -x

# WORK
# eDP-1 345mm x 194mm
# HDMI-2 597mm x 336mm

case "$1" in
ok)

	# Ideally, I'd use 3840x2160 + (2x2) 2560x1440 = 8960x2880 but 8192x8192 is max
	# So let's try 3840x2160 + (2x2) 2048x1152 = 7936x2304
	xrandr --dpi 280 --fb 7936x2304 \
		--output eDP-1 --mode 3840x2160 \
		--output HDMI-2 --scale 2x2 --pos 3840x0
;;
test|"")
	# Let's try 3200x1800 + (2x2) 2560x1440 = 8320x2880 doesn't fit
	# 2880x1620 + (2x2) 2560x1440 = 8000x2880?
	xrandr --dpi 210 --fb 8000x2880 \
		--output eDP-1 --mode 2880x1620 \
		--output HDMI-2 --scale 2x2 --pos 2880x0
	;;
big)
	# Ideally, I'd use 3840x2160 + (2x2) 2560x1440 = 8960x2880 but 8192x8192 is max
	xrandr --dpi 280 --fb 8950x2880 \
		#--output eDP-1 --mode 2880x1620 \
		--output HDMI-2 --scale 2x2 --pos 3840x0
	;;
desktop)
	# Most important is to use the desktop screen
	xrandr --dpi 108 \
		--output HDMI-2 --primary --mode 2560x1440 --pos 1920x0 \
		--output eDP-1 --mode 1920x1080 --pos 0x360
esac


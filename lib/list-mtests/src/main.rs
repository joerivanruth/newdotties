
use std::{env, path::Path};
use std::error::Error;
use std::fs;
use std::io::{BufRead,BufReader};
use std::path::PathBuf;

use walkdir::WalkDir;

fn main() -> Result<(), Box<dyn Error>> {
    let mut dirs_only = false;
    let mut full_dirs = false;
    for arg in env::args().skip(1) {
        if arg == "--dirs" {
            dirs_only = true
        } else if arg == "--full-dirs" {
            full_dirs = true;
        } else {
            return Err(format!("unexpected argument: {}", arg).into());
        }
    }

    let source_dir = Path::new(".");

    for prefix in all_test_dirs_below(&source_dir)? {
        let dir = source_dir.join(&prefix);
        let tests = tests_in_dir(&dir)?;

        let slash_tests = if full_dirs { "/Tests" } else { "" };
        println!("{}{}\t{} tests", prefix.display(), slash_tests, tests.len());
        if !dirs_only {
            for test in tests {
                println!("{}/{}", prefix.display(), test);
            }
        }
    }

    Ok(())
}

fn all_test_dirs_below(dir: &Path) -> Result<Vec<PathBuf>, Box<dyn Error>> {
    let walker = WalkDir::new(&dir)
        .max_open(50)
        .sort_by(|a,b| a.file_name().cmp(b.file_name()))
        ;

    let mut result = vec![];
    for entry in walker {
        let entry = entry?;

        if entry.metadata()?.is_dir() {
            continue;
        }

        let path = entry.into_path();
        if !path.ends_with("Tests/All") {
            continue;
        }

        let prefix = path
            .strip_prefix(&dir)
            .expect("found something outside MONETDB_DIR")
            .ancestors()
            .nth(2) // drop Tests/All
            .unwrap()
            .to_owned()
            ;

        result.push(prefix);
    }

    Ok(result)
}

fn tests_in_dir(dir: &Path) -> Result<Vec<String>, Box<dyn Error>> {
    let path = dir.join("Tests/All");
    let f = fs::File::open(&path)?;

    let mut result = vec![];
    for line in BufReader::new(f).lines() {
        let line = line?;
        let line = line.splitn(2, "#").nth(0).unwrap();
        let line = line.splitn(2, "?").last().unwrap();
        let line = line.trim();
        if line.len() == 0 {
            continue;
        }
        result.push(line.to_owned());
    }

    Ok(result)

}